const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {

	console.log(user);

	const data = {
		id: user._id,
		email: user.email,
		mobileNumber: user.mobileNumber,
		isAdmin: user.isAdmin
	}
		return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if (token != undefined){
		
		token = token.slice(7, token.length);

		console.log(token);

		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not valid
			if(err){
				return res.send({auth: "Invalid Token!"})
			}
			else {
				next();
			}
		})
	}
	else {
		res.send({message: "Auth failed! No token provided!"})
	}
};

// Token decryption
module.exports.decode = (token) => {
	if (token !== undefined) {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data)=>{
			if(err){
				// if token is not valid
				return null;
			}
			else {
				// Decode method is used to obtain the information from the JWT
				// Syntax: jwt.decode(token, [options]);
				// Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else {
		// token does not exist
		return null;
	}
}
