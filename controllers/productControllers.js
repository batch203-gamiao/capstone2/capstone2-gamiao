const Product = require("../models/Product");
const auth = require("../auth");


// Create products (Admin Only)
module.exports.addProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// If user is admin then save product	
	if(userData){
		if(userData.isAdmin){
			let newProduct = new Product({
				productId: userData.id,
				name: req.body.name,
				slug: req.body.slug,
				description: req.body.description,
				price: req.body.price,
				stocks: req.body.stocks
			})
				return newProduct.save()
					.then(product => {
						console.log(product);
						res.send({message: "You have successfully created a product", product});
					})
					.catch(err => {
						console.log(err);
						res.send(false);
					})
		}	
		// else then do not make access
		else {
			return res.status(401).send("You don't have access to this page!");
		}
	}
	else {
		return res.status(401).send("Administrator access only!");
	}
};

// Retrieve All Products (Admin only)
module.exports.getAllProducts = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	if(userData){
		if(userData.isAdmin){
			return Product.find({},{productOrders:0,createdOn:0,__v:0}).then(result => {
				res.send(result);
			})
		}
		else {
			return res.status(401).send("You don't have access to this page!");
		}
	}
	else {
		return res.status(401).send("Administrator access only!");
	}
	
};

// Retrieve All Active Products
module.exports.getAllActive = (req, res) => {
	return Product.find({isActive: true},{productOrders:0,createdOn:0,__v:0}).then(result => res.send(result));
};

// Retrieving specific products
module.exports.getProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	
	return Product.findOne({slug: req.params.slug},{isActive:0,createdOn:0,productOrders:0,__v: 0}).then(result => {
		if(userData){
			if(userData.isAdmin){
				res.send(result);
			}
			else if (userData.isAdmin == false && result.isActive){
				res.send(result);
			}
			else {
				return res.status(401).send("Cannot find specific product");
			}
		}
		else {
			if(result.isActive){
				console.log(userData)
				res.send(result);
			}
			else {
				console.log(userData);
				return res.status(401).send("Cannot find specific product");
			}
		}
	});
}

// Update products (Admin only)
module.exports.updateProduct = (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData){
		if (userData.isAdmin){
			let updateProduct = {
				name: req.body.name,
				slug: req.body.slug,
				description: req.body.description,
				price: req.body.price,
				stocks: req.body.stocks
			}
			if(updateProduct.name || updateProduct.slug){		
				return Product.find({slug: req.body.slug})
				.then(result => {

					console.log(result);

					// Slug already exists
					if(result.length > 0){
						return res.send("Slug already exists.")
					}
					// There are no duplicate found.
					else {
						Product.findByIdAndUpdate(req.params.productId, updateProduct, {new: true})
							.then(result => {	
								res.send({message:"Product updated successfully. You have changed the name / slug value. Be careful. Changing the name or slug value can have unintended side effects!",result});
							})
							.catch(error => {
								console.log(error);
								res.send(false);
							});
					}
				})
				.catch(error => res.send(error));
			}	
			else {
				return Product.find({slug: req.body.slug})
				.then(result => {

					console.log(result);

					// Slug already exists
					if(result.length > 0){
						return res.send("Slug already exists.")
					}
					// There are no duplicate found.
					else {
						Product.findByIdAndUpdate(req.params.productId, updateProduct, {new: true})
							.then(result => {	
								res.send({message:"Product updated successfully!",result});
							})
							.catch(error => {
								console.log(error);
								res.send(false);
							});
					}
				})
				.catch(error => res.send(error));
			}	
			
		}
		else {
			return res.status(401).send("You don't have access to this page!");
		}
	}
	else {
		return res.status(401).send("Administrator access only!");
	}
	
}

// Archive product (Admin only)
module.exports.archiveProduct = (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	
	let updateIsActiveField = {
		isActive: req.body.isActive
	}
	if(userData){
		if(userData.isAdmin){
			return Product.findByIdAndUpdate(req.params.productId, updateIsActiveField, {new: true})
			.then(result => {
				console.log(result);
				if(result.isActive) {
					res.send("The product has been added from the shop!");
				}
				else {
					res.send("The product has been removed from the shop!");
				}
			})
			.catch(error => {
				console.log(error);
				res.send("Product not found!");
			})
		}
		else {
			return res.status(401).send("You don't have access to this page!");
		}
	}
	else {
		return res.status(401).send("Administrator access only!");
	}
}

// Retrieve product order details (admin only)
module.exports.productOrderDetails = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		let user = Product.find( { }, { name:1, stocks:1, price:1,productOrders:1,_id: 0 })
		.then(result=>{
				res.send(result);
		});
	}
	else {
		res.send("You do not have any access for this page!");
	}
}

