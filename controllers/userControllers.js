const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password,10),
		mobileNumber: req.body.mobileNumber
	})

	console.log(newUser);

	if(userData){
		res.send("You are already logged in!");
	}
	else {

		return User.find({$or:[{email: req.body.email},{mobileNumber: req.body.mobileNumber}]})
		.then(result => {
			// If the email address already exist
			if(result.length > 0){
				return res.send("Email or Mobile Number already exists!")				
			}
			// If no email address exist then save user
			else {
				return newUser.save()
				.then(user => {
					console.log(user);
					res.send("The user has been registered successfully");
				})
				.catch(err => {
					console.log(err);
					res.send(false);
				});
			}
		})
		.catch(error => res.send(error));
	}	
}

// User Authentication
module.exports.loginUser = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData){
		res.send("You are already logged in!");
	}
	else {
		return User.findOne({email: req.body.email})
		.then(result => {
			// User does not exists
			console.log(result.email)
			if(result == null){
				// return res.send(false);
				return res.send("Email does not exist. Try again!");
			}
			// User exists
			else {
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
				// if the passwords match/result of the above code is true.
				if(isPasswordCorrect){
					return res.status(200).send({message: "Login Success!",accessToken: auth.createAccessToken(result)});
				}
				else {
					return res.send("Incorrect password!");
				}
			}
		})
		.catch(err => {
			res.send(err);
		});
	}	
}

// User Details
module.exports.userDetails = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData){
		let user = await User.findById(userData.id)
		.then(result=>result);
		let data = {
			id: user.id,
			fullName: user.firstName + " " + user.lastName,
			email: user.email,
			mobileNumber: user.mobileNumber,
			isAdmin: user.isAdmin
		}
		res.send(data);
	}
	else {
		res.send("You need to login first to access this page!");
	}
}

// Order Details
module.exports.orderDetails = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData){
		let user = await User.findById(userData.id)
		.then(result=>result);
		res.send(user.orders);
	}
	else {
		res.send("You need to login first to access this page!");
	}
}

// All Order Details (Admin only)
module.exports.allOrderDetails = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData){
		if(userData.isAdmin){
			let user = await User.find({isAdmin: false},{email:1 , mobileNumber:1,orders: 1,_id: 0})
			.then(result=>res.send(result));
			
		}
		else {
			res.send("You do not have access for this page!");
		}
		
	}
	else {
		res.send("You do not have access for this page!");
	}
}

// Setting user as admin (Admin only)
module.exports.setUserAdmin = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData){
		if (userData.isAdmin) {
		        return User.findById(req.body.userId)
		            .then(user => {
		                user.isAdmin = !user.isAdmin;
		                return user.save()
		                    .then(result => res.send({ success: "The role has been changed", Name: user.firstName + " " + user.lastName,isAdmin: user.isAdmin}))
		                    .catch(err => res.send({ message: "Not Updated", response: false }));
		            })
		    } else {
		        return res.send({ message: "You are not allowed to do this task.", response: false })
		    }

	}
	else {
		res.send("You need to login first to access this page!");
	}
}

// User cart
module.exports.getCart = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData){
		let user = await User.findById(userData.id)
		.then(result=>{
			if (result.cart.length > 0){
				res.send(result.cart);
			} 
			else {
				res.send("There is no product on this cart!")
			}
		});
		res.send(user);
	}
	else {
		res.send("You need to login first to access this page!");
	}
}


// User add product to cart
module.exports.addToCart = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);


	if(userData) {

		if(req.body.productId == null) {
			return res.send("Product Id is not defined!")
		};

		let product = await Product.findById(req.body.productId)
		.then(result => result)
		.catch(error => res.send("Product Id is not defined!"));

		let data = {
			productId: product.id,
			productName: product.name,
			totalPrice: product.price * req.body.quantity,
			quantity: req.body.quantity
		}

		if(userData.isAdmin){
			return res.status(401).send("You need to login as customer to access this page!");
		}
		else {
			return User.findById(userData.id)
			.then(result => {
				let indexProduct;
				let productExist;

				if (result.cart.length > 0) {
					productExist = result.cart.map((e,i) => {
						if (product.id === e.productId){
							indexProduct = i
							return true;
						}
						else {
							return false;
						}
					});
				}
				else {
					productExist = false;
				}

				if (productExist.length > 0) {
					productExist = productExist.some(e => e == true);
				}
				else {
					productExist = false;
				}

				if (productExist) {
					result.cart[indexProduct].quantity += req.body.quantity;
					result.cart[indexProduct].totalPrice += data.totalPrice;
					return result.save()
					.then(result => {
						res.send("Cart updated!"+"\n\n"+result.cart[indexProduct])
					})
					.catch(err => res.send(err));
				}
				else {
					if(product.stocks = req.body.quantity < 0) {
						return res.send(` Cannot add to cart! Remaining stocks left: ${product.productStocks}`)
					}
					result.cart.push(data);

					return result.save()
					.then(result => res.send("Added to Cart \n" + result.cart[result.cart.length -1]))
					.catch(err => res.send(err));
				}
			});
		}
	}
	else {
		return res.status(401).send("You need to login first to access this page!");
	}
}

// ===========================================================================================
// User cart modify quantity 
module.exports.modifyCartQuantity = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    return User.findById(userData.id)
        .then(user => {
            user.cart.forEach((e, i) => {
                if (e.cartNumber === req.body.cartNumber) {
                    let originalPrice = user.cart[i].totalPrice / user.cart[i].quantity;
                    user.cart[i].quantity = req.body.quantity;
                    user.cart[i].totalPrice = originalPrice * req.body.quantity;

                    return user.save()
                        .then(result => res.send({success: "Updated quantity"}))
                        .catch(err => res.send("Not Updated"));
                } else {
                    if (i === user.cart.length - 1) {
                        return res.send({ message: "Not Updated. Cart Number provided does not exist in the current user.", response: false })
                    }
                }
            });
        })
}

// User cart modify status to be included or not in the checkout
module.exports.modifyStatus = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    return User.findById(userData.id)
        .then(user => {
            user.cart.forEach((e, i) => {
                if (e.cartId === req.body.cartId) {
                    user.cart[i].status = !user.cart[i].status;

                    return user.save()
                        .then(result => res.send({ success: "Updated!"}))
                        .catch(err => res.send({ error: "Not Updated!"}));
                } else {
                    if (i === user.cart.length - 1) {
                         return res.send("Cart ID does not exist in cart.")
                    }
                }
            });
        })
}

// Delete product from cart
module.exports.deleteCart = (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    User.findById(userData.id)
        .then(user => {

        	user.cart.forEach((e,i)=> {
        		if (e.cartId === req.body.cartId){
        			
        			user.cart.splice(i, 1);
        			return user.save()
        			    .then(result => {
        			    	res.send("Product Removed Successfully")
        			    })
        			    .catch(err => res.send("Not Deleted"));
        		}
        		else {
        		        return res.send("Cart ID does not exist in cart.")
        		}
        	})

            /*let cart = [...user.cart];
            console.log(cart)

            cart.map((e, i) => {
                console.log(e.cartId);
                console.log("hello");
                if (e.cartId === req.body.cartId) {
                    user.cart.splice(i, 1);
                    user.save()
                        .then(result => {
                        	return	res.send("Product Removed Successfully")
                        })
                        .catch(err => res.send("Not Deleted"));
                } else {
                    return res.send("Cart ID does not exist in cart.")
                }
            });*/
        })
        .catch(err => res.send({ message: err.message, response: false }));
}

// Checkout products that has a status of true
module.exports.checkOut =  async (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    return await User.findById(userData.id)
    .then(user => {
    	let isCartNotEmpty = user.cart.length > 0

    	if(isCartNotEmpty) {
    		let statusCheck = user.cart.filter(e => e.status === true);

    		statusCheck.forEach(e => {
    			user.orders.push({
    				productId: e.productId,
                    quantity: e.quantity,
                    productName: e.productName,
                    totalPrice: e.totalPrice
    			});
    		})

    		let statusArr = statusCheck.length;

    		user.save()
    			.then(result => {
    				let userOrders = [...user.orders];
    				let newOrders = [];
    				for (let i = userOrders.length - statusArr; i < userOrders.length; i++) {
    				    newOrders.push(userOrders[i]);
    				}
    				let err = [];
    				let completed = [];
    				newOrders.forEach( async(e,i) => {
    					let isProductOrder = await Product.findById(e.productId)
    						.then(product => {    	
    													
    							if (product.stocks >= e.quantity) {
    								
    								product.productOrders.push({
    									orderId: e.orderId,
    									userEmail: userData.email,
    									userId: userData.id,
    									quantity: e.quantity,
    									totalPrice: e.totalPrice
    								});
    								product.stocks -= e.quantity;

    								console.log(product);

    								let cartId = e.cartId;
    								let cartIndexToRemove;

    								user.cart.forEach((el, i)=> {
    									if (el.cartId === cartId) {
    										cartIndexToRemove = i;
    									}
    								});

    								user.cart.splice(cartIndexToRemove, 1); 
    								product.save().then(result => result).catch(err => err);
    								user.save().then(result => result).catch(err => err);
    								completed.push({ productId: e.productId });
    							}
    							else {
    								user.orders.splice(orders.length - statusArr + i, 1);
    								user.save().then(result => result).catch(err => err);
    								err.push({ errorOnProductId: e.productId });
    							}
    							if (err.length > 0 && completed.length == 0) {
                                        return res.send("Encountered an error while placing order!");
                                    }
                                if (err.length > 0 && completed.length > 0) {
                                        return res.send("Some products has encountered an error while placing order!");
                                    }
                                if (err.length == 0 && completed.length > 0) {
                                        return res.send("All products has been successfully ordered!");
                                    }
    						})
    						.catch(err => err);
    				});
    			})
    			 .catch(err => res.send("Encountered an error while checking out"));
    	}
    	else {
    		return res.send("Encountered an error while checking out!");
    	}
    })
    .catch(err => {
            return res.send("No product in checkout!");
        });
}







