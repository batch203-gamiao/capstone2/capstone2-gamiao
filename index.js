// -------1
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 4000;
//Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
// 

// MongoDB connection ----2
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.zxhvj7d.mongodb.net/b203_ecommerceAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));
//

// middlewares-------3
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());
//

// routes------4
app.use("/users", userRoutes);
app.use("/products", productRoutes);
// 

//port ------5
app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})
// 
