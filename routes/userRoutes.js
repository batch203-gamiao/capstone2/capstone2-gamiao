const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// Route for user registration
router.post("/register", userControllers.registerUser);

// Route for user authentication
router.post("/login", userControllers.loginUser);

// Route for user details
router.get("/details", userControllers.userDetails);

// Route for order details
router.get("/order/details", userControllers.orderDetails);

// Route for all order details (admin only)
router.get("/order/details/all", userControllers.allOrderDetails);

// Route for setting user as admin
router.post("/details/edit", userControllers.setUserAdmin);

// Route for user cart
router.get("/cart", userControllers.getCart);

// Route for adding products to cart
router.post("/cart", userControllers.addToCart);

// Route for adding cart to checkout
router.post("/checkout", userControllers.checkOut);

// Route for updating quantity of products from cart
router.patch('/cart', userControllers.modifyCartQuantity);

// Route for deleting cart 
router.patch('/cart/remove',userControllers.deleteCart);

// Route for updating checkout
router.patch('/checkout', userControllers.modifyStatus);



module.exports = router;