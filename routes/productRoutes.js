const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating products (Admin only)
router.post("/", productControllers.addProduct);

// Route for viewing all products (Admin only)
router.get("/all", productControllers.getAllProducts);

// Route for viewing all active courses (all users)
router.get("/", productControllers.getAllActive);

// Route for viewing all product order details (Admin only)
router.get("/details", productControllers.productOrderDetails);

// Route for viewing specific products (all users)
router.get("/:slug", productControllers.getProduct);

// Route for updating specific products (Admin Only)
router.put("/:productId", productControllers.updateProduct);

// Route for archiving products from shop
router.patch("/archive/:productId",productControllers.archiveProduct);

module.exports = router;