const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email:{
		type: String,
		required: [true, "Email is required"]
	},
	password:{
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber:{
		type: String,
		required: [true, "Mobile number is required"]
	},
	cart: 
	[ 
		{
			cartId: {
				type: String,
				default: () => {
					let cartId = [];
					for (let i = 1; i <= 10; i++){
						let rand = Math.floor(Math.random() * 10);
						cartId.push(rand);
					}
					return cartId.join("");
				}
			},
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
			productName: {
				type: String,
				required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			totalPrice: {
				type: Number,
				required: [true, "Total Price is required"]
			},
			status: {
				type: Boolean,
				default: true
			},
			createdOn: {
				type: Date,
				default: new Date()
			}	
		}	
	],
	orders: 
	[
		{		
			productId: {
				type:String,
				required: [true, "Product Id is required"]
			},
			productName: {
				type:String,
				required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			totalPrice: {
				type: Number,
				required: [true, "Total Price is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	],

	isAdmin:{
		type: Boolean,
		default: false
	},
	createdOn:{
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("User", userSchema);
