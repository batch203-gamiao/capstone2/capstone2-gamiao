const mongoose = require("mongoose");
const slug = require('mongoose-slug-generator');

mongoose.plugin(slug);

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Product name is required"]
	},
	slug: {
		type: String, 
		slug: "name", 
		unique: true
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	stocks:{
		type: Number,
		required: [true, "Stocks is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	productOrders: 
	[
		{	
			orderId: {
				type: String,
				default: () => {
					let cartId = [];
					for (let i = 1; i <= 11; i++){
						let rand = Math.floor(Math.random() * 10);
						cartId.push(rand);
					}
					return cartId.join("");
				}
			},		
			userId : {
				type: String, 
				required: [true, "User Id is required"]
			},
			userEmail : {
				type: String, 
				required: [true, "User email is required"]
			},
			quantity : {
				type: Number, 
				required: [true, "Quantity is required"]
			},
			totalPrice: {
				type: Number,
				required: [true, "Total Price is required"]

			},
			purchasedOn : {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);